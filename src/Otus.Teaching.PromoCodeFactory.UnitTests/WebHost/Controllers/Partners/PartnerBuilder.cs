﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System.Collections.Generic;
using System;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class PartnerBuilder
    {
        private Partner _partner = new Partner();

        public PartnerBuilder WithBaseProperties()
        {
            _partner = new Partner()
            {
                Id = Guid.NewGuid(),
                Name = "TestName",
                IsActive = true,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
            };

            return this;
        }

        public PartnerBuilder WithNotActiveLimit()
        {
            _partner.PartnerLimits = new List<PartnerPromoCodeLimit>
            {
                new PartnerPromoCodeLimit()
                {
                    Id = Guid.NewGuid(),
                    CreateDate = new DateTime(2022, 12, 05),
                    EndDate = DateTime.Now.AddDays(-1),
                    CancelDate = DateTime.Now.AddDays(-1),
                    Limit = 100
                }
            };

            return this;
        }

        public PartnerBuilder WithActiveLimit()
        {
            _partner.NumberIssuedPromoCodes = 50;
            _partner.PartnerLimits = new List<PartnerPromoCodeLimit>
            {
                new PartnerPromoCodeLimit()
                {
                    Id = Guid.NewGuid(),
                    CreateDate = new DateTime(2022, 12, 05),
                    EndDate = DateTime.Now.AddDays(1),
                    Limit = 100
                }
            };

            return this;
        }

        public PartnerBuilder WithNumberIssuedPromoCodes(int number)
        {
            _partner.NumberIssuedPromoCodes = number;

            return this;
        }

        public Partner Create()
        {
            return _partner;
        }
    }

}