﻿using AutoFixture.AutoMoq;
using AutoFixture;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using System;
using Xunit;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using FluentAssertions;
using System.Collections.Generic;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly PartnersController _partnersController;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsyncTests_PartnerIsNotFound_ReturnNotFound()
        {
            var autoFixture = new Fixture();

            // Arrange
            var partnerId = Guid.NewGuid();
            Partner partner = null;

            var partnerPromoCodeLimitRequest = autoFixture.Build <SetPartnerPromoCodeLimitRequest>().Create();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, partnerPromoCodeLimitRequest);

            // Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotActive_ReturnsBadRequest()
        {
            //Arrange
            var autoFixture = new Fixture();

            var partnerPromoCodeLimitRequest = autoFixture.Build<SetPartnerPromoCodeLimitRequest>().Create();

            var partner = new PartnerBuilder()
                .WithBaseProperties()
                .Create();

            partner.IsActive = false;

            _partnersRepositoryMock.Setup(x => x.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, partnerPromoCodeLimitRequest);

            //Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }


        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerHasActiveLimit_NumberIssuedPromoCodesShouldBeZero()
        {
            //Arrange
            var autoFixture = new Fixture();

            var partnerPromoCodeLimitRequest = autoFixture.Build<SetPartnerPromoCodeLimitRequest>().Create();

            var partner = new PartnerBuilder()
                .WithBaseProperties()
                .WithNumberIssuedPromoCodes(100)
                .WithActiveLimit()
                .Create();

            _partnersRepositoryMock.Setup(x => x.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            //Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, partnerPromoCodeLimitRequest);

            //Assert
            partner.NumberIssuedPromoCodes.Should().Be(0);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerPreviousLimitIsEnd_NumberIssuedPromoCodesShouldNotBeZero()
        {
            //Arrange
            var autoFixture = new Fixture();

            var partnerPromoCodeLimitRequest = autoFixture.Build<SetPartnerPromoCodeLimitRequest>().Create();

            var partner = new PartnerBuilder()
                .WithBaseProperties()
                .WithNumberIssuedPromoCodes(100)
                .WithNotActiveLimit()
                .Create();

            _partnersRepositoryMock.Setup(x => x.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            //Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, partnerPromoCodeLimitRequest);

            //Assert
            partner.NumberIssuedPromoCodes.Should().NotBe(0);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerHasActiveLimit_CancelPreviousLimit()
        {
            var autoFixture = new Fixture();

            var partnerPromoCodeLimitRequest = autoFixture.Build<SetPartnerPromoCodeLimitRequest>().Create();

            var partner = new PartnerBuilder()
                .WithBaseProperties()
                .WithNumberIssuedPromoCodes(100)
                .WithActiveLimit()
                .Create();

            _partnersRepositoryMock.Setup(x => x.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            //Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, partnerPromoCodeLimitRequest);

            //Assert
            partner.PartnerLimits.First().CancelDate.Should().HaveValue();
        }

        /// <summary>
        /// Лимит должен быть больше 0
        /// </summary>
        [Theory]
        [InlineData(0)]
        [InlineData(-1)]

        public async void SetPartnerPromoCodeLimitAsync_LimitLessOrEqualsZero_ReturnsBadRequest(int limit)
        {
            var autoFixture = new Fixture();

            var partnerPromoCodeLimitRequest = autoFixture.Build<SetPartnerPromoCodeLimitRequest>()
                .With(request => request.Limit, limit)
                .Create();

            var partner = new PartnerBuilder()
                .WithBaseProperties()
                .Create();

            _partnersRepositoryMock.Setup(x => x.GetByIdAsync(partner.Id))
               .ReturnsAsync(partner);

            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, partnerPromoCodeLimitRequest);

            //Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_LimitIsValid_SaveSuccessfully()
        {
            var autoFixture = new Fixture();

            var partnerPromoCodeLimitRequest = autoFixture.Build<SetPartnerPromoCodeLimitRequest>()
                .Create();

            var partner = new PartnerBuilder()
                .WithBaseProperties()
                .Create();

            _partnersRepositoryMock.Setup(x => x.GetByIdAsync(partner.Id))
               .ReturnsAsync(partner);

            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, partnerPromoCodeLimitRequest);

            //Assert
            _partnersRepositoryMock.Verify(repos => repos.UpdateAsync(partner), Times.Once);
        }
    }
}